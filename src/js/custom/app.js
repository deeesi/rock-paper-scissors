const RADIO_CHECKED_STR = 'input[name="userChoice"]:checked';
const RADIO_STR = 'input[name="userChoice"]';
const LABEL_STR = 'label[attr="attr"]';

document.onreadystatechange = function() {
	if (document.readyState === 'complete') {
		let userButton = document.getElementById('userChoiceBtn');
		let userChoiceRadios = document.querySelectorAll(RADIO_STR);
		let userChoiceLabel = document.querySelectorAll(LABEL_STR);
		let form = document.getElementById('sendUserChoice');

		form.addEventListener('submit', function(event) {
			event.preventDefault();
			return false;
		});
		userButton.addEventListener('click', choose);

		for (let i = 0; i < userChoiceLabel.length; i++) {
			userChoiceLabel[i].addEventListener('click', function(event) {
				removeActiveStateTiles();
				this.classList.add('active');
			});
		}
		handleImprint();
	}
};

function removeActiveStateTiles() {
	let userChoiceLabel = document.querySelectorAll(LABEL_STR);
	let userChoiceRadios = document.querySelectorAll(RADIO_STR);

	for (let i = 0; i < userChoiceLabel.length; i++) {
		userChoiceLabel[i].classList.remove('active');
		userChoiceRadios[i].checked = false;
	}
}

function resetActiveStateTiles() {
	let userChoiceLabel = document.querySelectorAll(LABEL_STR);
	let userChoiceRadios = document.querySelectorAll(RADIO_STR);

	for (let i = 0; i < userChoiceLabel.length; i++) {
		userChoiceLabel[i].classList.add('active');
		userChoiceRadios[i].checked = false;
	}
}

function getRandomChoice(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function handleImprint() {
	document.getElementById('showImprint').addEventListener('click', function(event) {
		document.getElementById('imprintContainer').classList.add('imprint-container--visible');
	});
	document.getElementById('closeImprint').addEventListener('click', function(event) {
		document.getElementById('imprintContainer').classList.remove('imprint-container--visible');
	});
}

function choose() {
	let userChoiceValue;
	let randomChoice = getRandomChoice(0, 2);
	let randomChoiceValue;
	let classes = ['result--rock', 'result--paper', 'result--scissors'];

	for (let i = 0; i < classes.length; i++) {
		document.getElementById('userChoice').classList.remove(classes[i]);
		document.getElementById('computerChoice').classList.remove(classes[i]);
	}

	if (document.querySelector(RADIO_CHECKED_STR) != null) {
		userChoiceValue = document.querySelector(RADIO_CHECKED_STR).value;
	}

	if (randomChoice === 0) {
		randomChoiceValue = 'rock';
	} else if (randomChoice === 1) {
		randomChoiceValue = 'paper';
	} else {
		randomChoiceValue = 'scissors';
	}

	document.getElementById('resultContainer').classList.add('result--visible');
	document.getElementById('userChoice').classList.add('result--' + userChoiceValue);
	document.getElementById('computerChoice').classList.add('result--' + randomChoiceValue);
	document.querySelector('.result__text').classList.add('result__text--visible');
	document.querySelector('.final-text').classList.add('final-text--visible');
	compare(userChoiceValue, randomChoiceValue);

	document.getElementById('closeResult').addEventListener('click', function(event) {
		document.getElementById('resultContainer').classList.remove('result--visible');
		resetActiveStateTiles();
	});
}

function compare(choice1, choice2) {
	if (choice1 === choice2) {
		document.getElementById('finalText').innerHTML = 'The result is a tie!';
	} else if (choice1 === 'rock') {
		if (choice2 === 'scissors') {
			document.getElementById('finalText').innerHTML = 'Rock wins. <br>YOU win!';
		} else {
			document.getElementById('finalText').innerHTML = 'Paper wins. <br>Computer wins :-(';
		}
	} else if (choice1 === 'paper') {
		if (choice2 === 'rock') {
			document.getElementById('finalText').innerHTML = 'Paper wins. <br>YOU win!';
		} else {
			document.getElementById('finalText').innerHTML = 'Scissors wins.<br> Computer wins :-(';
		}
	} else if (choice1 === 'scissors') {
		if (choice2 === 'rock') {
			document.getElementById('finalText').innerHTML = 'Rock wins. <br>Computer wins :-(';
		} else {
			document.getElementById('finalText').innerHTML = 'Scissors wins. <br>YOU win!';
		}
	}
}
